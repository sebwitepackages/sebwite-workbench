<?php

namespace Sebwite\Workbench\Console;

use Sebwite\Support\Util;

class AddCommand extends BaseCommand
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add
                            {method=false : clone | new}
                            {--N|name=false : (new) The "vendor/package" name}
                            {--O|org=false : (clone) A github organisation/username}
                            {--R|repo=false : (clone) The repository name}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new workbench package.';

    public function handle()
    {
        $method = $this->argument('method');
        if ( $method === 'false' )
        {
            $method = $this->choice('What to do?', [
                'clone' => 'Clone a package from github',
                'new'   => 'Generate a new package'
            ]);
        }

        if ( $method === 'clone' )
        {
            $this->addClone();
        }
        elseif ( $method === 'new' )
        {
            $this->addNew();
        }
        else
        {
            $this->error('Not a valid method: ' . $method);
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    protected function addNew()
    {
        $this->emit('adding');

        $name = $this->option('name');
        if ( $name === 'false' )
        {
            $name = $this->ask('The "vendor/package" name');
        }

        $wb = $this->getWorkbench();

        # Create package
        try
        {
            $package = $wb->packages->create($name);
        }
        catch (\Exception $e)
        {
            $this->error($e->getMessage());

            return;
        }

        $files = $this->config('generator.files');
        foreach ( $files as $from => $to )
        {
            $to = Util::template($to, $package->generator->getGeneratorVars());
            $files[ $from ] = $to;
        }

        # Generate files
        $package->generator->generate($files);

        # Add pre-commit hook
        $package->git->addPreCommitHook();

        # Initial commit
        if ( $this->confirm('Create initial commit?', true) )
        {
            $package->shell([ 'git commit -m "Initial commit"' ]);
        }

        # Merge composer
        if ( $this->confirm('Merge with root composer?') )
        {
            $package->composer->merge();
            $this->getWorkbench()->composer->run('dumpautoload');
        }

        if ( $this->confirm('Do you want to create or define a remote?') )
        {
            $this->createGitRemote($package);
        }

        $this->emit('added');

        # Done
        $this->line('All done sire!');
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    protected function addClone()
    {
        $this->emit('cloning');

        # Github or bitbucket?
        $remoteName = $this->chooseRemote();
        if ( $remoteName === false )
        {
            return $this->error('Could not use that remote. It\'ts not configured');
        }

        # Pick organisation / owner
        $org = $this->option('org');
        if ( $org === 'false' )
        {
            $org = $this->chooseOrganisation();
        }

        # Pick repository
        $repo = $this->option('repo');
        if ( $repo === 'false' )
        {
            $repo = $this->chooseRepository($org);
        }

        # Pick branch
        $branch = $this->chooseBranch($org, $repo);

        # Check if composer.json is found
        $fs              = $this->getRemote()->getFilesystem($repo, $org, $branch);
        $hasComposerJson = $fs->exists('composer.json');

        # Any sensible PHP package/project should have composer.json.
        # But just in case, have noobfallback in place
        if ( !$hasComposerJson )
        {
            $this->warn("Could not find composer.json in [{$org}/{$repo}@{$branch}].");
            if ( !$this->confirm('Are you sure you want to continue?') )
            {
                return;
            }
            $packageName = $this->ask('Clone into dir [vendor/package]', "{$org}/{$repo}");
        }
        else
        {
            $this->comment('Downloading composer.json');
            $composer    = json_decode($fs->get('composer.json'), true);
            $packageName = $composer[ 'name' ];
        }

        # Validate package name
        if ( !$this->isValidPackageName($packageName) )
        {
            return $this->error('Invalid name [' . $packageName . ']');
        }

        $packageDir = $this->getWorkbench()->path($packageName);

        # Ensure directory does not exist
        if ( $this->fs->exists($packageDir) )
        {
            $this->error('Destination directory already exists');

            return $this->error("Could not clone the repository into [{$packageDir}]");
        }

        # Clone the package
        $this->comment('Cloning the repository');
        $url = $remoteName === 'github' ? 'github.com' : 'bitbucket.org';

        #return $this->error($url);
        $this->getWorkbench()->shell([
            "git clone https://{$url}/{$org}/{$repo} {$packageDir}"
        ]);

        # Get the package wrapper class
        $package = $this->getWorkbench()->packages->get($packageName);

        # Merge composer with root composer
        if ( $hasComposerJson && !$package->composer->isMerged() )
        {
            $package->composer->merge();
        } # Or if no composer, check if user wants to create
        elseif ( !$hasComposerJson && $this->confirm('No composer.json was found, do you want to add it now? (recommended)', true) )
        {
            $package->shell('composer init');
        }

        # Ensure build.xml file
        if ( !$package->phing->hasBuildFile() )
        {
            $this->line('No build.xml file detected, adding to package');
            $package->phing->ensureBuildFile();
        }

        # Ask user if he wants pre-commit hooks
        if ( !$package->git->hasPreCommitHook() && $this->confirm('Do you want to add the build.xml pre-commit hook? (recommended)', true) )
        {
            $package->git->addPreCommitHook();
            $this->line('Added pre-commit hook to execute build.xml');
        }

        # Update zeh stuff
        $this->line('Dumping autoload..');
        $this->getWorkbench()->composer->run('dumpautoload');

        if ( $this->confirm('Fire composer update? Recommended when the package has any "require" dependencies.', false) )
        {
            $this->getWorkbench()->composer->run('update');
        }

        $this->emit('cloned');

        $this->line('All done sire!');
    }
}
