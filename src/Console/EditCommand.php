<?php

namespace Sebwite\Workbench\Console;


class EditCommand extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'edit
                            {action=false : The action you want to execute }
                            {name=false : The "vendor/package" name }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Alter a package in the workbench.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        if ($name === 'false') {
            $name = $this->selectPackage();
        }
        $package = $this->getWorkbench()->packages->get($name);

        $actions = [
            'hook'    => 'Enable phing build.xml pre-commit hook',
            'unhook'  => 'Disable phing build.xml  pre-commit hook',
            'merge'   => 'Merge into root composer.json',
            'unmerge' => 'Unmerge out of root composer.json',
            'remote'  => 'Change the remote git repository',
            'commit'  => 'Commit all changes',
            'bump'    => 'Bump/tag package version',
            'remove'  => 'Remove'
        ];

        $unset[] = $package->git->hasPreCommitHook() ? 'hook' : 'unhook';
        $unset[] = $package->composer->isMerged() ? 'merge' : 'unmerge';

        array_forget($actions, $unset);

        $action = $this->argument('action');
        if ($action === 'false') {
            $action = $this->choice('What do you want to do?', $actions);
        }


        switch ($action) {
            case 'hook':
                $package->phing->ensureBuildFile();
                $package->git->addPreCommitHook() ? $this->line('Added pre-commit hook') : $this->error('Could add pre-commit hook');
                break;
            case 'unhook':
                $package->git->removePreCommitHook() ? $this->line('Removed pre-commit hook') : $this->error('Could not remove pre-commit hook');
                break;
            case 'merge':
                $package->composer->merge() ? $this->line('Merged into root composer') : $this->error('Could not merge into root composer');
                break;
            case 'unmerge':
                $package->composer->unmerge() ? $this->line('Unmerged out of root composer') : $this->error('Could not unmerge out of root composer');
                break;
            case 'remote':
                $this->createGitRemote($package);
                break;
            case 'commit':
                $this->call(config('workbench.console_prefix') . ':commit', [ 'name' => $name ]);
                break;
            case 'bump':
                $this->call(config('workbench.console_prefix') . ':bump', [ 'name' => $name ]);
                break;
            case 'remove':
                if ($package->composer->isMerged()) {
                    $package->composer->unmerge() ? $this->comment('Unmerged out of root composer') : $this->warn('Could not unmerge out of root composer');
                    if ($this->confirm('Update composer?', true)) {
                        $this->getWorkbench()->composer->run('update');
                        $this->comment('Composer updated');
                    }
                }
                app('files')->deleteDirectory($package->path()) ? $this->comment('Removed package') : $this->error('Could not remove package');
                break;
            default:
                return $this->error('The action you specified does not exist: ' . $action);
                break;
        }

    }
}
