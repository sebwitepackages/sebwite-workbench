<?php

namespace Sebwite\Workbench\Console;

use Sebwite\Workbench\Packages\Package;
use vierbergenlars\SemVer\version;

class BumpCommand extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bump
                            {name=false : The "vendor/package" name }
                            {type=false : patch | minor | major }
                            {--push=origin : The remote to push to}
                            {--all : The remote to push to}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Bump version and tag it for a workbench package.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if($this->option('all')){
            $message  = $this->ask('Commit message?', 'Commit on ' . time());
            $push     = $this->confirm('Push to remote?', true);

            $version = new version('0.0.1');
            $bumpType = $this->argument('type');
            if ($bumpType === 'false') {
                $bumpType = $this->choice('Type of version bump?', [
                    'patch' => 'Patch',
                    'minor' => 'Minor',
                    'major' => 'Major',
                ], 'patch');
            }

            echo $bumpType;
            foreach($this->getWorkbench()->packages->all() as $package){
                #$this->dump($package->git->getCountedChanges());
                $this->bumpPackage($package, $bumpType, 'Mass tag', true);
                $this->comment("Package [{$package->getName()}] bumped");
            }
            $this->info('All done sire!');
            return;
        }
        # Select package
        $name = $this->argument('name');
        if ($name === 'false') {
            $name = $this->selectPackage();
        }
        $package = $this->getWorkbench()->packages->get($name);

        # Select bump type (patch, minor, major)
        $version = $this->getPackageVersion($package);
        $bumpType = $this->argument('type');
        if ($bumpType === 'false') {
            $bumpType = $this->choice('Type of version bump?', [
                'patch' => 'Patch to ' . $version->inc('patch')->valid(),
                'minor' => 'Minor to ' . $version->inc('minor')->valid(),
                'major' => 'Major to ' . $version->inc('major')->valid(),
            ], 'patch');
        }

        $message  = $this->ask('Commit message?', 'Commit on ' . time());
        $push     = $this->confirm('Push to remote?', true);
        $this->bumpPackage($package, $bumpType, $message, $push);
        $this->info('All done sire!');
    }

    protected function bumpPackage(Package $package, $bumpType, $message, $push)
    {
        $tagName  = $this->getPackageVersion($package)->inc($bumpType)->valid();
        $commands = [
            'git add -A',
            'git commit -m "' . $message . '"',
            'git tag -a ' . $tagName . ' -m "' . $message . '"'
        ];
        if ($push) {
            $commands[] = 'git push -u ' . $this->option('push') . ' ' . $tagName;
        }

        $package->shell($commands);
    }

    protected function getPackageVersion(Package $package)
    {
        $version = $package->git->version();
        try {
            $version = new version($version);

        } catch (\Exception $e) {
            $version = new version('0.0.0');
        }
        return $version;
    }
}
