<?php

namespace Sebwite\Workbench\Console;

use Sebwite\Workbench\Console\Traits\GitPackageCommandTrait;

class ListCommand extends BaseCommand
{
    use GitPackageCommandTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'list {--more : Show more information}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lists all workbench packages with some additional information.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $header = [ 'Name', 'Version', 'Branch', 'Git', 'Hooked', 'Merged' ];
        $rows   = [ ];
        foreach ( $this->getWorkbench()->packages->all() as $package )
        {
            if(!$package->git->isUsingGit()){
                continue;
            }
            $status = $this->getStatus($package);
            $git           = $package->git;
            $version       = $git->version();
            $validVersion  = $this->getWorkbench()->validateVersion($version);
            $preCommitHook = $git->repository()->getHooks()->has('pre-commit');
            $row           = [
                $package->getName(),
                $validVersion ? $this->style('bold', $version) : $this->style('dark_gray', $version),
                $git->branch(),
                implode('::', $status),
                $preCommitHook ? $this->style('green', 'Yes') : '',
                $package->composer->isMerged() ? $this->style('green', 'Yes') : '',
                #app('files')->exists($package->path('extension.php')) ? $this->style('green', 'Yes') : ''
            ];
            if ( $this->option('more') )
            {
                $targets = 0;
                if ( $package->phing->hasBuildFile() )
                {
                    #$phing   = $package->phing->project();
                    $targets = count($package->phing->targets());
                }
                $row[] = "{$targets}";
            }

            $rows[] = $row;
        }
        if ( $this->option('more') )
        {
            $header[] = 'Phing';
        }
        $this->table($header, $rows);

        $legenda = [
            $this->style('yellow', 'modified'),
            $this->style('green', 'added'),
            $this->style('blue', 'untracked'),
            $this->style('red', 'deleted')
        ];
        $this->line('Git status: [' . implode('  ', $legenda) . ']');
    }
}
