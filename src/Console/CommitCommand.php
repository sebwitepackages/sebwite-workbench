<?php

namespace Sebwite\Workbench\Console;

class CommitCommand extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commit
                            {name=false : The "vendor/package" name }
                            {--push=origin : The remote to push to}
                            {--all : Will go and commit all packages}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Commit changes for the specified workbench package.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ( $this->option('all') )
        {
            $name = [ ];
            foreach ( $this->getWorkbench()->packages->all() as $package )
            {
                if ( empty($package->git->getCountedChanges()) )
                {
                    continue;
                }
                $name[] = $package->getName();
                #$this->info('Commiting: ' . $package->getName());
                #$this->call('wb:commit', [ 'name' => $package->getName() ]);
            }
            #return $this->comment('All done sire');
        }
        else
        {

            $name = $this->argument('name');
            if ( $name === 'false' )
            {
                $name = $this->selectPackage(true);
            }
        }
        $message = $this->ask('Commit message?', 'Commit on ' . time());
        $push    = $this->confirm('Push to remote?', true);

        if ( is_string($name) )
        {
            $name = [ $name ];
        }

        foreach ( $name as $_name )
        {
            $package = $this->getWorkbench()->packages->get($_name);
            $this->comment('Please hold on ...');
            $commands = [
                'git add -A',
                'git commit -m "' . $message . '"'
            ];
            if ( $push )
            {
                $commands[] = 'git push -u ' . $this->option('push') . ' ' . $package->git->branch();
            }
            $package->shell($commands);
            $this->info("Committed {$_name}");
        }
        $this->info('All done sire!');
    }
}
