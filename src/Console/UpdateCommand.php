<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Workbench\Console;


class UpdateCommand extends BaseCommand
{
    protected $signature = 'update';

    protected $description = 'Updates all workbench pages from remote';

    public function handle()
    {
        /** @var \Sebwite\Workbench\Packages\Package[] $update */
        $update = [ ];
        foreach ( $this->getWorkbench()->packages->all() as $package )
        {
            $package->git->fetch(function ($hasUpdates, $output) use (&$package, &$update)
            {
                $name = $package->getName();
                if ( in_array($name, $update, true) )
                {
                    return;
                }
                $out = $hasUpdates ? $this->style([ 'red' ], 'is not up to date') : $this->style([ 'green' ], 'is up to date');
                if ( $hasUpdates )
                {
                    if(!in_array($package, $update))
                    {
                        $update[] = $package;
                    }
                }
                $this->writeln("[{$name}] {$out}");
            }, true);
        }

        foreach ( $update as $package )
        {
            if ( $this->confirm('Would you like to update [' . $package->getName() . ']', true) )
            {
                $package->git->update(function ($err, $out)
                {
                    $this->line($err);
                    $this->line($out);
                });
            }
        }
    }
}
