<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Workbench\Console\Traits;


use Sebwite\Workbench\Packages\Package;

trait GitPackageCommandTrait
{
    public function getStatus(Package $package)
    {
        $git     = $package->git;
        $changes = $git->getCountedChanges();
        $status  = [ ];
        if ( !empty($changes) )
        {
            foreach ( $changes as $name => $i )
            {
                if ( is_string($name) && is_int($i) && $i > 0 )
                {
                    switch ( $name )
                    {
                        case 'modified':
                            $status[] = $this->style('yellow', $i);
                            break;
                        case 'added':
                            $status[] = $this->style('green', $i);
                            break;
                        case 'untracked':
                            $status[] = $this->style('blue', $i);
                            break;
                        case 'deleted':
                            $status[] = $this->style('red', $i);
                            break;
                    }
                }
            }
        }

        return $status;
    }
}
