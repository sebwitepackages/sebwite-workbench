<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * MIT License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Workbench\Console\Traits;

use Sebwite\Git\Console\GitConsoleTrait;
use Sebwite\Support\Str;
use Sebwite\Workbench\Packages\Package;

/**
 * This is the WorkbenchTrait trait.
 *
 * @package   Sebwite\Dev
 * @author    Sebwite Dev Team
 * @copyright Copyright (c) 2015, Sebwite
 * @license   https://tldrlegal.com/license/mit-license MIT License
 * @mixin \Sebwite\Support\Console\Command
 */
trait RemoteCommandTrait
{
    use GitConsoleTrait;


    protected function createGitRemote(Package $package)
    {

        $choice = $this->choice('Create or define?', [ 'create', 'define' ]);

        # Github or bitbucket?
        $remoteName = $this->chooseRemote();
        if ( $remoteName === false )
        {
            return $this->error('Could not use that remote. It\'ts not configured');
        }

        # Pick organisation / owner
        if ( array_key_exists('org', $this->option()) )
        {
            $org = $this->option('org');
        }

        if ( ! isset($org) || $org === 'false' )
        {
            $org = $this->chooseOrganisation();
        }

        $repo = Str::replace($package->getName(), '/', '-');
        $repo = Str::replace($repo, '\\', '-');
        $repo = $this->ask('Name of the repository?', $repo);

        if ( $choice === 'create' )
        {
            $private = $this->confirm('Private? ', false);
            $this->getRemote()->createRepository($repo, compact('private'), $org);
        }

        $url = $remoteName === 'bitbucket' ? 'https://bitbucket.org' : 'https://github.com';
        $url = $this->ask('Remote url ok?', "{$url}/{$org}/{$repo}");

        $package->git->setGitRemote('origin', $url);
    }
}
