<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * MIT License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Workbench\Console\Traits;

/**
 * This is the WorkbenchTrait trait.
 *
 * @package   Sebwite\Dev
 * @author    Sebwite Dev Team
 * @copyright Copyright (c) 2015, Sebwite
 * @license   https://tldrlegal.com/license/mit-license MIT License
 * @mixin \Sebwite\Support\Console\Command
 */
trait WorkbenchCommandTrait
{

    /**
     * @var \Sebwite\Workbench\Workbench
     */
    protected $workbench;

    /**
     * getWorkbench
     *
     * @return \Sebwite\Workbench\Workbench
     */
    protected function getWorkbench()
    {
        if ( null === $this->workbench )
        {
            $this->workbench = $this->getLaravel()->make('workbench');
        }

        return $this->workbench;
    }

    /**
     * selectPackage
     *
     * @return string
     */
    protected function selectPackage($multiple = false)
    {
        $list = [ ];
        foreach ( $this->getWorkbench()->packages->all() as $package )
        {
            $list[] = $package->getName();
        }

        return $this->choice(
            $multiple ? 'Select 1 or more packages seperated by ,' : 'Select package',
            $list,
            null,
            null,
            $multiple
        );
    }

    /**
     * config method
     *
     * @param null $key
     * @param null $default
     *
     * @return array|mixed
     */
    public function config($key = null, $default = null)
    {
        return $this->getWorkbench()->config($key, $default);
    }
}
