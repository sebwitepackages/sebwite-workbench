<?php

namespace Sebwite\Workbench\Console;

use Sebwite\Support\Str;
use Sebwite\Workbench\Console\Traits\GitPackageCommandTrait;

class InfoCommand extends BaseCommand
{
    use GitPackageCommandTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'info
                            {name=false : The "vendor/package" name }
                            {--print-remote : Print the git remote }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show information about a package in the workbench.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        if ( $name === 'false' )
        {
            $name = $this->selectPackage();
        }
        $package = $this->getWorkbench()->packages->get($name);

        if ( $this->option('print-remote') )
        {
            return $package->shell('git remote -v');
        }

        $usesPhing    = $this->yesno($package->phing->hasBuildFile());
        $commitHook   = $this->yesno($package->git->hasPreCommitHook());
        $isExtension  = $this->yesno(app('files')->exists($package->path('extension.php')));
        $phingTargets = $usesPhing ? implode(', ', $package->phing->targets()) : '';
        $autoloads    = collect($package->composer->autoloads())->keys()->transform(function ($v)
        {
            return Str::removeRight($v, '\\');
        })->implode(',');
        $gitStatus    = implode('::', $this->getStatus($package));

        print <<<EOF
{$this->bold('package')}         : {$this->style('bold', $package->getName())}
{$this->bold('branch')}          : {$package->git->branch()}
{$this->bold('version')}         : {$package->git->version()}
{$this->bold('path')}            : {$package->path()}
{$this->bold('commit hook')}     : {$commitHook}
{$this->bold('is extension')}    : {$isExtension}
{$this->bold('uses phing')}      : {$usesPhing} ({$phingTargets})
{$this->bold('autoloads')}       : {$this->style('yellow', $autoloads)}
{$this->bold('git status')}      : {$gitStatus}

EOF;
        $this->fireEvent('command.workbench.info.after', [ $this ]);
    }

    protected function bold($str)
    {
        return $this->style('bold', $str);
    }

    protected function yesno($bool)
    {
        return $bool ? $this->style('green', 'yes') : $this->style('red', 'no');
    }
}
