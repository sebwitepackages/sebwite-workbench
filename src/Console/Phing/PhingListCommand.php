<?php

namespace Sebwite\Workbench\Console\Phing;

/**
 * This is the BaseCommand.
 *
 * @package        Sebwite\Dev
 * @author         Sebwite Dev Team
 * @copyright      Copyright (c) 2015, Sebwite
 * @license        https://tldrlegal.com/license/mit-license MIT License
 */
class PhingListCommand extends PhingCommand
{
    use \Sebwite\Workbench\Traits\PhingTrait;

    protected $signature = 'phing:list {name=false : The "vendor/package" name }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all phing tasks in a package.';

    public function handle()
    {
        $name = $this->argument('name');
        if ($name === 'false') {
            $name = $this->selectPackage();
        }
        $package = $this->getWorkbench()->package($name);
        $phing   = $package->phing->project();

        /** @var \Target[] $target */
        $targets = $phing->getTargets();

        $rows = [];
        foreach ($targets as $target) {
            if (strlen($target->getName()) < 2) {
                continue;
            }
            $rows[] = [$target->getName(), $target->getDescription()];
        }
        $this->table(['Target', 'Description'], $rows);
    }
}
