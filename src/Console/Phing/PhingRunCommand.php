<?php

namespace Sebwite\Workbench\Console\Phing;

/**
 * This is the BaseCommand.
 *
 * @package        Sebwite\Dev
 * @author         Sebwite Dev Team
 * @copyright      Copyright (c) 2015, Sebwite
 * @license        https://tldrlegal.com/license/mit-license MIT License
 */
class PhingRunCommand extends PhingCommand
{
    use \Sebwite\Workbench\Traits\PhingTrait;

    protected $signature = 'phing:run
                            {name=false : The "vendor/package" name }
                            {task=false : The phing task to run }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run a phing task for a package';

    public function handle()
    {
        $name = $this->argument('name');
        if ($name === 'false') {
            $name = $this->selectPackage();
        }
        $package = $this->getWorkbench()->package($name);
        $target  = $this->selectPhingTargetFor($package);
        $this->runPhingTargetFor($package, $target);
    }
}
