<?php

namespace Sebwite\Workbench\Console\Phing;

use Sebwite\Workbench\Console\BaseCommand;

/**
 * This is the BaseCommand.
 *
 * @package        Sebwite\Dev
 * @author         Sebwite Dev Team
 * @copyright      Copyright (c) 2015, Sebwite
 * @license        https://tldrlegal.com/license/mit-license MIT License
 */
abstract class PhingCommand extends BaseCommand
{
    use \Sebwite\Workbench\Traits\PhingTrait;
}
