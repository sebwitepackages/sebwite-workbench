<?php
/**
 * Part of the Sebwite PHP Packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Workbench\Console;

/**
 * This is the WatchCommand.
 *
 * @package        Sebwite\Workbench
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite
 */
class ClearCommand extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears all routes, views, cache, etc. Then dumps autoload';

    public function handle()
    {
        $this->call('route:clear');
        $this->call('view:clear');
        $this->call('cache:clear');
        $this->call('config:clear');
        $this->call('clear-compiled');
        $this->getWorkbench()->composer->run('dumpautoload');
    }
}
