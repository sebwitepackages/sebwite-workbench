<?php

namespace Sebwite\Workbench\Console;

use Sebwite\Idea\Contracts\Idea;
use Sebwite\Support\Console\Command;
use Sebwite\Support\Str;
use Sebwite\Support\Traits\NamespacedPackageTrait;

/**
 * This is the BaseCommand.
 *
 * @package        Sebwite\Dev
 * @author         Sebwite Dev Team
 * @copyright      Copyright (c) 2015, Sebwite
 * @license        https://tldrlegal.com/license/mit-license MIT License
 */
abstract class BaseCommand extends Command
{
    use NamespacedPackageTrait;
    use Traits\WorkbenchCommandTrait;
    use Traits\RemoteCommandTrait;

    protected static $callbacks = [ ];

    protected $idea;

    /**
     * @var \Sebwite\Support\Filesystem
     */
    protected $fs;

    /**
     * Create a new console command instance.
     *
     * @param \Sebwite\Idea\Contracts\Idea $idea
     *
     * @internal param \Sebwite\Support\Filesystem $fs
     *
     * @internal param \Sebwite\Workbench\Factory $workbench
     */
    public function __construct()
    {
        $this->fs        = app('files');
        $prefix          = config('workbench.console_prefix');
        $this->signature = Str::ensureLeft($this->signature, $prefix . ':');
        parent::__construct();
    }

    /**
     * @return \Sebwite\Idea\Contracts\Idea|\Sebwite\Idea\Idea
     */
    public function idea()
    {
        return $this->getLaravel()->make('sebwite.idea');
    }


    protected function hasPHPStorm()
    {
        return $this->getLaravel()->bound('phpstorm');
    }

    protected function phpstorm()
    {
        return $this->getLaravel()->make('phpstorm');
    }

    public static function on($name, \Closure $callback)
    {
        if ( !array_key_exists($name, static::$callbacks) )
        {
            static::$callbacks[ $name ] = [ ];
        }
        static::$callbacks[ $name ][] = $callback;
    }

    protected function emit()
    {
        $params = func_get_args();
        $name   = array_shift($params);
        if ( array_key_exists($name, static::$callbacks) )
        {
            foreach ( static::$callbacks[ $name ] as $callback )
            {
                /** @var \Closure $callback */
                $callback->call($this, $params);
            }
        }
    }
}
