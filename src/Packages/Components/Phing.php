<?php
/**
 * Part of the Docit PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Workbench\Packages\Components;

use Sebwite\Support\Filesystem;
use Sebwite\Workbench\Packages\Component;
use Sebwite\Workbench\Packages\Package;

/**
 * This is the class Phing.
 *
 * @package        Sebwite\Workbench
 * @author         Docit
 * @copyright      Copyright (c) 2015, Docit. All rights reserved
 */
class Phing extends Component
{
    protected $files;

    public function __construct(Package $parent, Filesystem $files)
    {
        parent::__construct($parent);
        $this->files = $files;
    }


    #
    # PHING

    /**
     * getPhing
     *
     * @param string $filename
     * @return \Project
     */
    public function project($filename = 'build.xml')
    {
        \Phing::startup();
        $project = new \Project();
        $project->setBasedir($this->path());
        $file = $project->resolveFile($filename);
        \Phing::setCurrentProject($project);
        $project->init();
        \ProjectConfigurator::configureProject($project, $file);

        return $project;
    }

    public function getTargets()
    {
        return $this->project()->getTargets();
    }

    public function targets()
    {
        return collect($this->project()->getTargets())->transform(function ($target) {
            return $target->getName();
        })->toArray();
    }

    /**
     * Checks if the package has a build.xml file
     *
     * @return bool
     */
    public function hasBuildFile()
    {
        return $this->files->exists($this->path('build.xml'));
    }

    /**
     * Generates a build.xml file if the package doesn't have one
     */
    public function ensureBuildFile()
    {
        if (! $this->hasBuildFile()) {
            $this->generator->generate([ 'build.xml.stub' => 'build.xml' ]);
        }
    }

    /**
     * Removes the build.xml file
     */
    public function removeBuildFile()
    {
        if ($this->hasBuildFile()) {
            $this->files->remove($this->path('build.xml'));
        }
    }
}
