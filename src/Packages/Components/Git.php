<?php
/**
 * Part of the Docit PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Workbench\Packages\Components;

use PHPGit\Command\StatusCommand;
use Sebwite\Git\Contracts\Manager;
use Sebwite\Support\Filesystem;
use Sebwite\Support\Traits\NamespacedPackageTrait;
use Sebwite\Support\Util;
use Sebwite\Workbench\Packages\Component;
use Sebwite\Workbench\Packages\Package;

/**
 * This is the class Phing.
 *
 * @package        Sebwite\Workbench
 * @author         Docit
 * @copyright      Copyright (c) 2015, Docit. All rights reserved
 */
class Git extends Component
{
    use NamespacedPackageTrait;

    protected $files;

    protected $generator;

    protected $repo;

    /**
     * Git constructor.
     *
     * @param \Sebwite\Workbench\Packages\Package $package
     * @param \Sebwite\Support\Filesystem         $files
     * @param \Sebwite\Git\Manager                $git
     */
    public function __construct(Package $parent, Filesystem $files, Manager $git)
    {
        parent::__construct($parent);
        $this->files = $files;
        $this->repo  = $git->local($this->path());
    }


    /**
     * getVersion
     *
     * @return string
     */
    public function version()
    {
        try
        {
            return $this->git()->describe('HEAD', [ 'always' => true ]);
        }
        catch (\Exception $e)
        {
            return '';
        }
    }

    /**
     * getBranch
     *
     * @return mixed
     */
    public function branch()
    {
        return $this->repository()->getHead()->getName(); //last(explode('/', $this->repo->getHead()->getFullname()));
    }

    public function getCountedChanges()
    {
        $status = $this->git()->status();
        if ( count($status[ 'changes' ]) === 0 )
        {
            return '';
        }

        $changes         = [ ];
        $constantsToName = [ ];
        $class           = new \ReflectionClass(StatusCommand::class);
        $constants       = $class->getConstants();
        foreach ( $constants as $name => $val )
        {
            $changes[ strtolower($name) ] = 0;
            $constantsToName[ $val ]      = $name;
        }

        foreach ( $status[ 'changes' ] as $file )
        {
            if ( $file[ 'work_tree' ] === 'o' )
            {
                continue;
            }
            $key = strtoupper($file[ 'work_tree' ]);
            if ( !isset($constantsToName[ $key ]) )
            {
                continue;
            }
            $name = $constantsToName[ $key ];
            $changes[ strtolower($name) ]++;
        }

        return $changes;
    }

    /**
     * gitInit
     *
     * @return $this
     */
    public function init()
    {
        $this->shell([
            'git init',
            'git add -A'
        ]);

        return $this;
    }

    /**
     * git
     *
     * @return \PHPGit\Git
     */
    public function git()
    {
        $git = new \PHPGit\Git();
        $git->setRepository($this->path());

        return $git;
    }

    public function setGitRemote($name, $url)
    {
        $this->shell([
            'git remote rm ' . $name,
            'git remote add ' . $name . ' ' . $url
        ]);
    }

    /**
     * repository
     *
     * @return \Gitonomy\Git\Repository
     */
    public function repository()
    {
        return $this->repo;
    }

    /**
     * addPreCommitHook
     */
    public function addPreCommitHook()
    {
        $preCommit = <<<HOOK
#!/bin/bash
../../../vendor/bin/phing -f build.xml pre-commit
git add -A
HOOK;

        $this->repo->getHooks()->set('pre-commit', $preCommit);

        return $this->hasPreCommitHook() === true;
    }

    /**
     * hasPreCommitHook
     *
     * @return bool
     */
    public function hasPreCommitHook()
    {
        return $this->repo->getHooks()->has('pre-commit');
    }

    /**
     * removePreCommitHook
     */
    public function removePreCommitHook()
    {
        $this->repo->getHooks()->remove('pre-commit');

        return $this->hasPreCommitHook() === false;
    }

    public function fetch($callback, $dryRun = true)
    {
        $command = config('workbench.git.fetch');
        if($dryRun){
            $command = "$command --dry-run";
        }
        $this->shell($command, null, [
            'callback' => function ($err, $out) use ($callback)
            {
                $a = preg_match_all('/\[up to date\]/', $out);
                call_user_func_array($callback, [ $a === 0, $out, $err ]);
            }
        ]);
    }

    public function update($callback)
    {
        $branch = $this->branch();
        $command = Util::template(config('workbench.git.update'), compact('branch'));
        $this->shell($command, null, compact('callback'));
    }

    public function isUsingGit()
    {
        $path = $this->path('.git');
        return $this->files->exists($path) && $this->files->isDirectory($path);
    }
}
