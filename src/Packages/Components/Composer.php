<?php
/**
 * Part of the Docit PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Workbench\Packages\Components;

use Sebwite\Support\Filesystem;
use Sebwite\Support\Path;
use Sebwite\Workbench\Packages\Component;
use Sebwite\Workbench\Packages\Package;

/**
 * This is the class Composer.
 *
 * @package        Sebwite\Workbench
 * @author         Docit
 * @copyright      Copyright (c) 2015, Docit. All rights reserved
 *
 */
class Composer extends Component
{
    protected $files;

    protected $defaultPath;

    public function __construct(Package $parent, Filesystem $files)
    {
        parent::__construct($parent);
        $this->files = $files;
        $this->setDefaultPath($this->getWorkbench()->config('composer.package_file'));
    }

    /**
     * @return mixed
     */
    public function getDefaultPath()
    {
        return $this->defaultPath;
    }

    /**
     * Set the composerFile value
     *
     * @param mixed $defaultPath
     *
     * @return Composer
     */
    public function setDefaultPath($defaultPath)
    {
        $this->defaultPath = $this->getPath($defaultPath);

        return $this;
    }

    protected function getPath($path = null){
        return $path === null ? $this->getDefaultPath() : $this->path($path);
    }

    /**
     * Checks if the package is merged into the root composer.json. Uses the wikimedia merge-plugin.
     *
     * @return bool
     */
    public function isMerged($path = null)
    {
        if ( ! $includes = $this->getWorkbench()->composer->get('extra.merge-plugin.include', false) )
        {
            return false;
        }
        $path = Path::makeRelative($this->getPath($path), base_path());

        return in_array($path, $includes, true);
    }

    /**
     * Adds this package into the root composer.json. Uses the wikimedia merge-plugin.
     *
     * @return $this|bool
     */
    public function merge($path = null)
    {
        if ( $this->isMerged() )
        {
            return false;
        }

        $path = Path::makeRelative($this->getPath($path), base_path());

        $merge = [
            'replace' => [ $this->getName() => '*' ],
            'extra'   => [ 'merge-plugin' => [ 'include' => [ ] ] ]
        ];

        $data = array_replace_recursive($this->getWorkbench()->composer->get(), $merge);
        if ( ! in_array($path, $data[ 'extra' ][ 'merge-plugin' ][ 'include' ], true) )
        {
            $data[ 'extra' ][ 'merge-plugin' ][ 'include' ][] = $path;
        }

        $this->getWorkbench()->composer->merge($data);

        return $this;
    }

    /**
     * Removes the package from the root composer.json. Uses the wikimedia merge-plugin.
     *
     * @return $this
     * @throws \Exception
     */
    public function unmerge($path = null)
    {
        if ( ! $this->isMerged() )
        {
            return false;
        }

        $rootComposer = $this->getWorkbench()->composer->get();
        $path         = Path::makeRelative($this->getPath($path), base_path());

        foreach ( $rootComposer[ 'extra' ][ 'merge-plugin' ][ 'include' ] as $k => $filePath )
        {
            if ( $filePath === $path )
            {
                unset($rootComposer[ 'extra' ][ 'merge-plugin' ][ 'include' ][ $k ]);
                break;
            }
        }

        if ( isset($rootComposer[ 'replace' ][ $this->getName() ]) )
        {
            unset($rootComposer[ 'replace' ][ $this->getName() ]);
        }

        return $this->getWorkbench()->composer->set($rootComposer);
    }

    /**
     * Returns the composer.json value using the key with dot notation
     *
     * @param      $key
     * @param null $default
     * @return mixed
     */
    public function get($key = null, $default = null, $path = null)
    {
        $data = json_decode($this->files->get($this->getPath($path)), true);

        return $key === null ? $data : array_get($data, $key, $default);
    }

    public function set($key, $value, $path = null)
    {
        $data = $this->get();
        array_set($data, $key, $value);
        $this->files->put($this->getPath($path), json_encode($data, JSON_PRETTY_PRINT));
    }

    public function autoloads($dev = false)
    {
        $autoloads = [];
        $loaders = [$this->get('autoload.psr-4', [])];
        if($dev){
            $loaders[] = $this->get('autoload-dev.psr-4', []);
        }
        foreach($loaders as $loader)
        {
            foreach ( $loader as $namespace => $path )
            {
                $autoloads[$namespace] = $path;
            }
        }
        return $autoloads;
    }
}
