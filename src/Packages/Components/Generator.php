<?php
/**
 * Part of the Docit PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Workbench\Packages\Components;

use Sebwite\Support\Filesystem;
use Sebwite\Support\StubGenerator;
use Sebwite\Support\Traits\NamespacedPackageTrait;
use Sebwite\Support\Util;
use Sebwite\Workbench\Packages\Component;
use Sebwite\Workbench\Packages\Package;

/**
 * This is the class Phing.
 *
 * @package        Sebwite\Workbench
 * @author         Docit
 * @copyright      Copyright (c) 2015, Docit. All rights reserved
 */
class Generator extends Component
{
    use NamespacedPackageTrait;

    protected $files;

    protected $generator;

    public function __construct(Package $parent, Filesystem $files, StubGenerator $generator)
    {
        parent::__construct($parent);
        $this->files     = $files;
        $this->generator = $generator;
    }


    /**
     * generate
     *
     * @param array $files
     * @param array $vars
     */
    public function generate(array $files = [ ], array $vars = [ ])
    {
        $vars     = array_replace_recursive($this->getGeneratorVars(), $vars);
        $this->generator->generate(
            $this->getWorkbench()->config('generator.stubs_path'),
            $this->path(),
            $files,
            $vars
        );
    }

    /**
     * getVars
     *
     * @param null $packageName
     *
     * @return array
     */
    public function getGeneratorVars()
    {
        $parsedNames = $this->parsePackageName($this->getName());
        $namespace   = $this->getPackageNamespace($this->getName());

        $stubDir = $this->getWorkbench()->config('stubs_path');
        $destDir = $this->path();

        $vars = [
            'package_path'   => $this->getPackage()->path(),
            'sep'            => DIRECTORY_SEPARATOR,
            'config'         => array_dot($this->getWorkbench()->config()),
            'open'           => '<?php',
            'stubDir'        => $stubDir,
            'destDir'        => $destDir,
            'packageName'    => $this->getName(),
            'vendor'         => $parsedNames[ 'vendor' ],
            'package'        => $parsedNames[ 'package' ],
            'namespace'      => $namespace,
            'testNamespace'  => head(explode('\\', $namespace)) . '\\Tests\\' . last(explode('\\', $namespace)),
            'classPrefix'    => last(explode('\\', $namespace)),
            'configFileName' => $parsedNames[ 'vendor' ] . '.' . $parsedNames[ 'package' ]
        ];

        return $vars;
    }
}
