<?php
/**
 * Part of the Docit PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Workbench\Packages;

use BadMethodCallException;
use Sebwite\Support\Traits\Extendable;

/**
 * This is the class Component.
 *
 * @package        Sebwite\Workbench
 * @author         Docit
 * @copyright      Copyright (c) 2015, Docit. All rights reserved
 * @mixin Package
 */
abstract class Component
{
    use Extendable;

    protected $package;

    /** Instantiates the class */
    public function __construct(Package $parent)
    {
        $this->package = $parent;
    }

    /**
     * @return \Sebwite\Workbench\Packages\Package
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * getContainer method
     *
     * @return \Illuminate\Container\Container
     */
    public function getContainer()
    {
        return \Illuminate\Container\Container::getInstance();
    }


    public function __call($method, $params)
    {
        $pkg = $this->package;

        if ( array_key_exists($method, static::$extensions) )
        {
            return $this->callExtension($method, $params);
        }
        elseif ( method_exists($pkg, $method) || array_key_exists($method, $pkg::extensions()) )
        {
            return call_user_func_array([ $pkg, $method ], $params);
        }

        throw new BadMethodCallException("Method [$method] does not exist.");
    }

}
