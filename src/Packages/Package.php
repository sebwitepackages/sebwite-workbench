<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Workbench\Packages;

use Illuminate\Contracts\Container\Container;
use Sebwite\Support\Path;
use Sebwite\Support\Traits\Extendable;
use Sebwite\Workbench\Contracts\Workbench as WorkbenchContract;
use SplFileInfo;

/**
 * This is the class Package.
 *
 * @package        Sebwite\Workbench
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 *
 *
 * @property \Sebwite\Workbench\Packages\Components\Composer  $composer
 * @property \Sebwite\Workbench\Packages\Components\Phing     $phing
 * @property \Sebwite\Workbench\Packages\Components\Generator $generator
 * @property \Sebwite\Workbench\Packages\Components\Git       $git
 */
class Package
{
    use Extendable;

    protected $name;

    protected $dir;

    protected $container;

    protected $workbench;

    public function __construct(Container $container, WorkbenchContract $workbench, $name, SplFileInfo $dir)
    {
        $this->container = $container;
        $this->workbench = $workbench;
        $this->name      = $name;
        $this->dir       = $dir;
    }


    /**
     * Runs the given shell commands in the package directory
     *
     * @param string|array $commands
     * @param null         $paths
     * @param array        $opts
     */
    public function shell($commands,$paths = null, array $opts = [])
    {
        $cwd = getcwd();
        chdir($this->path());
        $this->workbench->shell($commands ,$paths, $opts);
        chdir($cwd);
    }

    /**
     * Get the patch to the package directory
     *
     * @param null|string $path
     *
     * @return string
     */
    public function path($path = null)
    {
        $dirPath = $this->dir->getRealPath();
        if ( $path !== null )
        {
            $dirPath = Path::join($dirPath, $path);
        }

        return $dirPath;
    }

    #
    # Getters and setters
    #

    /**
     * get name value
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * get dir value
     *
     * @return \SplFileInfo
     */
    public function getDir()
    {
        return $this->dir;
    }

    /**
     * get workbench value
     *
     * @return \Sebwite\Workbench\Workbench
     */
    public function getWorkbench()
    {
        return $this->workbench;
    }

    /**
     * get container value
     *
     * @return \Illuminate\Contracts\Container\Container
     */
    public function getContainer()
    {
        return $this->container;
    }


}
