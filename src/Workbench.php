<?php
namespace Sebwite\Workbench;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\View\Factory as View;
use Sebwite\Support\Filesystem;
use Sebwite\Support\Traits\Bootable;
use Sebwite\Support\Traits\ConfigTrait;
use Sebwite\Support\Traits\ContainerTrait;
use Sebwite\Support\Traits\Extendable;
use Sebwite\Support\Traits\NamespacedPackageTrait;
use Sebwite\Support\Traits\PathTrait;
use Sebwite\Support\Util;
use Sebwite\Workbench\Console\Make;
use Sebwite\Workbench\Contracts\Workbench as WorkbenchContract;
use Symfony\Component\Finder\Finder;
use vierbergenlars\SemVer\version;

/**
 * This is the Factory.
 *
 * @package        Sebwite\Dev
 * @author         Sebwite Dev Team
 * @copyright      Copyright (c) 2015, Sebwite
 * @license        https://tldrlegal.com/license/mit-license MIT License
 *
 *
 * @property \Sebwite\Workbench\Components\Composer $composer
 * @property \Sebwite\Workbench\Components\Packages $packages
 */
class Workbench implements WorkbenchContract
{
    use
        ContainerTrait,
        ConfigTrait,
        PathTrait,
        Extendable,
        Bootable;

    /**
     * @var \Illuminate\Contracts\Filesystem\Filesystem|\Sebwite\Support\Filesystem
     */
    protected $fs;

    /** @var \Illuminate\Contracts\View\View */
    protected $view;

    /** @var  string The workbench directory relative path */
    protected $dir;

    /**
     * @var \Sebwite\Workbench\Packages\Collection
     */
    protected $packages;

    protected $configVars = [ ];

    /**
     * Workbench constructor.
     *
     * @param \Illuminate\Contracts\Container\Container $container
     * @param \Illuminate\Contracts\Events\Dispatcher   $dispatcher
     * @param \Sebwite\Support\Filesystem               $files
     * @param \Illuminate\Contracts\View\Factory        $view
     * @param \Illuminate\Contracts\Config\Repository   $config
     */
    public function __construct(
        Container $container,
        Dispatcher $dispatcher,
        Filesystem $files,
        View $view,
        Repository $config
    )
    {

        $this->setContainer($container);
        static::setDispatcher($dispatcher);
        $this->setConfig($config->get('workbench'));
        $this->fs   = $files;
        $this->view = $view;
        $this->dir  = $this->config('workbench_dir');
        $this->setPath(base_path($this->dir));
        $this->bootIfNotBooted();
    }

    public function config($key = null, $default = null)
    {
        return $key === null ? $this->config : array_get($this->config, $key, $default);
    }


    /**
     * Check if the given string is a valid SemVer
     *
     * @param $versionString
     *
     * @return bool|string
     */
    public function validateVersion($versionString)
    {
        try
        {
            $valid = (new version($versionString))->valid();
        }
        catch (\Exception $e)
        {
            $valid = false;
        }

        return $valid;
    }

    /**
     * Execute a shell command
     *
     * @param      $commands
     * @param bool $path
     */
    public function shell($commands, $path = null, array $opts = [ ])
    {
        return Util::shell($commands, array_replace_recursive([
            'cwd' => $path
        ], $opts));
    }

    /**
     * get workbenchDir value
     *
     * @return string
     */
    public function getDir()
    {
        return $this->dir;

    }
}
