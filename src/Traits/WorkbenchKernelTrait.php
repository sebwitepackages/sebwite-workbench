<?php
/**
 * Part of the Docit PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Workbench\Traits;

use Sebwite\Workbench\Workbench;

/**
 * This is the class WorkbenchKernelTrait.
 *
 * @package        Sebwite\Workbench
 * @author         Docit
 * @copyright      Copyright (c) 2015, Docit. All rights reserved
 * @mixin \App\Console\Kernel
 */
trait WorkbenchKernelTrait
{
    protected function getArtisan()
    {
        $artisan = parent::getArtisan();

        if ( env('APP_ENV', 'production') !== 'production' )
        {
            $artisan->resolveCommands(Workbench::$makeCommands);
        }

        return $artisan;
    }


}
