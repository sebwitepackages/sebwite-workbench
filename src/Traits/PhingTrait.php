<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * MIT License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Workbench\Traits;

use Sebwite\Workbench\Packages\Package;

/**
 * This is the WorkbenchTrait trait.
 *
 * @package   Sebwite\Dev
 * @author    Sebwite Dev Team
 * @copyright Copyright (c) 2015, Sebwite
 * @license   https://tldrlegal.com/license/mit-license MIT License
 * @mixin \Sebwite\Support\Console\Command
 */
trait PhingTrait
{
    protected function selectPhingTargetFor(Package $package)
    {
        $targets = $package->phing->project()->getTargets();
        $choices = [ ];
        foreach ($targets as $target) {
            if (strlen($target->getName()) < 2) {
                continue;
            }
            $choices[ $target->getName() ] = $target->getDescription();
        }

        return $this->choice('Pick a target', $choices);
    }

    protected function runPhingTargetFor(Package $package, $targets)
    {
        if (is_array($targets)) {
            $package->phing->project()->executeTargets($targets);
        } else {
            $package->phing->project()->executeTarget($targets);
        }
    }
}
