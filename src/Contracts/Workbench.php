<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * MIT License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Workbench\Contracts;

/**
 * Interface Workbench
 *
 * @package        Sebwite\Workbench
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 * @mixin \Sebwite\Workbench\Workbench
 *
 *
 * @property \Sebwite\Workbench\Components\Composer $composer
 */
interface Workbench
{
}
