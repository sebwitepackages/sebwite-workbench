<?php
/**
 * Part of the Sebwite PHP Packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Workbench\Components;

use Sebwite\Support\Filesystem;
use Sebwite\Support\Path;
use Sebwite\Support\Str;
use Sebwite\Workbench\Contracts\Workbench as WorkbenchContract;

/**
 * This is the Composer.
 *
 * @package        Sebwite\Workbench
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite
 */
class Composer extends Component
{
    /**
     * @var \Sebwite\Support\Filesystem
     */
    protected $fs;

    protected $defaultPath;


    /**
     * {@inheritDoc}
     */
    public function __construct(WorkbenchContract $parent, Filesystem $fs)
    {
        parent::__construct($parent);
        $this->fs = $fs;
        $this->setDefaultPath($this->config('composer.project_file'));
    }

    /**
     * @return mixed
     */
    public function getDefaultPath()
    {
        return $this->defaultPath;
    }

    /**
     * Set the composerFile value
     *
     * @param mixed $defaultPath
     *
     * @return Composer
     */
    public function setDefaultPath($defaultPath)
    {
        $this->defaultPath = $this->getPath($defaultPath);

        return $this;
    }

    public function getPath($path = null){
        $path = $path === null ? $this->getDefaultPath() : $path;
        $path = Str::startsWith($path, base_path()) ? $path : base_path($path);

        return $path;
    }


    /**
     * Read root composer.json file
     *
     * @param null $path
     *
     * @return array
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function read($path = null)
    {
        return json_decode($this->fs->get($this->getPath($path)), true);
    }

    /**
     * write
     *
     * @param array $data
     * @param null  $path
     *
     * @return $this
     * @throws \ErrorException
     */
    protected function write(array $data, $path = null)
    {
        if ( !$this->validate($data) )
        {
            throw new \ErrorException("Composer data not valid");
        }
        $this->fs->put($this->getPath($path), json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES, 4));

        return $this;
    }

    /**
     * Merge data into root composer.json file
     *
     * @param array $composerData
     *
     * @return $this
     */
    public function merge(array $composerData, $path = null)
    {
        $currentData = $this->read($path);
        $this->write(array_replace_recursive($currentData, $composerData), $path);

        return $this;
    }

    /**
     * Validates the given array with the composer schema
     *
     * @param array $data
     *
     * @return bool
     */
    public function validate(array $data)
    {
        return true;
    }

    /**
     * Runs the given composer command(s) in the root project directory
     *
     * @param $command
     *
     * @return $this
     */
    public function run($command)
    {
        $args = func_get_args();
        $cwd  = getcwd();
        chdir(base_path());

        if ( func_num_args() === 1 and is_array($args[ 0 ]) )
        {
            $args = $args[ 0 ];
        }

        foreach ( $args as $i => $arg )
        {
            passthru('composer ' . $arg);
        }

        chdir($cwd);

        return $this;
    }

    public function get($key = null, $default = null, $path = null)
    {
        return $key === null ? $this->read($path) : array_get($this->read($path), $key, $default);
    }

    public function set($key, $value = null, $path = null)
    {
        if ( is_array($key) && $value === null )
        {
            $this->write($key, $path);
        }
        else
        {
            $data = $this->read($path);
            $data = array_set($data, $key, $value);
            $this->write($data, $path);
        }

        return $this;
    }

    public function getAutoloads()
    {

        $autoloads = [ ];
        $loads = $this->get('autoload.psr-4', [], 'composer.json');
        foreach ( $loads as $namespace => $dir )
        {
            $namespace               = Str::removeRight($namespace, '\\');
            $path                    = base_path($dir);
            $autoloads[ $namespace ] = [
                'dir'      => Str::remove($dir, DIRECTORY_SEPARATOR),
                'path'     => $path,
                'relative' => Path::makeRelative($path, base_path()),
                'composer' => $this->getPath('composer.json'),
                'relativeComposer' => Path::makeRelative($this->getPath('composer.json'), base_path())
            ];
        }

        foreach ( $this->getWorkbench()->packages->all() as $package )
        {
            if(!$this->fs->exists($package->path('composer.json'))){
                continue;
            }
            /** @var \Sebwite\Workbench\Packages\Package $package */
            $loads = $package->composer->get('autoload.psr-4', []);
            foreach ( $loads as $namespace => $dir )
            {
                $namespace               = Str::removeRight($namespace, '\\');
                $path                    = $package->path($dir);
                $autoloads[ $namespace ] = [
                    'dir'      => Str::remove($dir, DIRECTORY_SEPARATOR),
                    'path'     => $path,
                    'relative' => Path::makeRelative($path, base_path()),
                    'composer' => $package->composer->getDefaultPath(),
                    'relativeComposer' => Path::makeRelative($package->composer->getDefaultPath(), base_path())
                ];
            }
        }

        return collect($autoloads);
    }
}
