<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Workbench\Components;


use Exception;
use InvalidArgumentException;
use Sebwite\Support\Filesystem;
use Sebwite\Support\Path;
use Sebwite\Support\Str;
use Sebwite\Support\Traits\NamespacedPackageTrait;
use Sebwite\Workbench\Contracts\Workbench;
use Symfony\Component\Finder\Finder;

class Packages extends Component
{
    use NamespacedPackageTrait;

    protected $fs;

    protected $items;

    /**
     * {@inheritDoc}
     */
    public function __construct(Workbench $parent, Filesystem $fs)
    {
        parent::__construct($parent);
        $this->fs    = $fs;
        $this->items = collect();
        $this->resolve();
    }

    /**
     * Resolve packages and create the package collection
     *
     * @param bool $removePrevious
     *
     * @return \Sebwite\Workbench\Packages\Collection
     */
    public function resolve($removePrevious = false)
    {
        if ( $removePrevious || null === $this->items )
        {
            $this->items = collect(); // $this->getContainer()->make('workbench.packages.collection');
        }
        $dirs = Finder::create()->in($this->path())->directories()->depth('== 1')->followLinks();
        foreach ( $dirs as $dir )
        {
            $name = Str::removeLeft($dir->getPathname(), $this->path() . '/');
            if ( $this->items->has($name) )
            {
                continue;
            }

            $package   = $this->getContainer()->make('workbench.packages.package', compact('workbench', 'name', 'dir')); //(app(), $this, $name, $dir);

            $this->items->put($name, $package);
        }

        return $this->items;
    }

    /**
     * package method
     *
     * @param $packageName
     *
     * @return \Sebwite\Workbench\Packages\Package
     */
    public function get($packageName)
    {
        if ( !$this->items->has($packageName) )
        {
            return $this->resolve()->get($packageName);
        }

        /** @var \Sebwite\Workbench\Packages\Package $package */
        $package = $this->items->get($packageName, false);

        return $package;
    }

    /**
     * createPackage
     *
     * @param $packageName
     *
     * @return \Sebwite\Workbench\Packages\Package
     * @throws \Exception
     */
    public function create($packageName)
    {
        if ( !$this->isValidPackageName($packageName) )
        {
            throw new Exception("Not a valid package name [{$packageName}]");
        }
        $path = Path::join($this->path(), $packageName);

        if ( $this->fs->exists($path) || $this->items->has($packageName) )
        {
            throw new InvalidArgumentException("Package [{$packageName}] already exists");
        }

        $this->fs->makeDirectory($path, 0755, true);

        $this->shell([
            'git init',
            'git add -A'
        ], $path);


        return $this->get($packageName);
    }

    # GETTERS/SETTERS

    /**
     * getPackages
     *
     * @return \Sebwite\Workbench\Packages\Package[]
     */
    public function all()
    {
        return $this->items;
    }
}
