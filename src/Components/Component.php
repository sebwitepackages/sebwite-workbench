<?php
/**
 * Part of the Docit PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Workbench\Components;

use BadMethodCallException;
use Sebwite\Support\Traits\Extendable;
use Sebwite\Workbench\Contracts\Workbench as WorkbenchContract;
use Sebwite\Workbench\Workbench;

/**
 * This is the class Component.
 *
 * @package        Sebwite\Workbench
 * @author         Docit
 * @copyright      Copyright (c) 2015, Docit. All rights reserved
 * @mixin Workbench
 */
abstract class Component
{
    use Extendable;


    /**
     * @var \Sebwite\Workbench\Contracts\Workbench|\Sebwite\Workbench\Workbench
     */
    protected $workbench;

    /**
     * Component constructor.
     *
     * @param \Sebwite\Workbench\Contracts\Workbench $workbench
     */
    public function __construct(WorkbenchContract $workbench)
    {
        $this->workbench = $workbench;
    }

    /**
     * @return WorkbenchContract|Workbench
     */
    public function getWorkbench()
    {
        return $this->workbench;
    }

    public function getContainer()
    {
        return \Illuminate\Container\Container::getInstance();
    }


    public function __call($method, $params)
    {
        $wb = $this->workbench;

        if ( array_key_exists($method, static::$extensions) )
        {
            return $this->callExtension($method, $params);
        }
        elseif ( method_exists($wb, $method) || array_key_exists($method, $wb::extensions()) )
        {
            return call_user_func_array([ $wb, $method ], $params);
        }

        throw new BadMethodCallException("Method [$method] does not exist.");
    }
}
