<?php


use Sebwite\Support\Path;

if (! function_exists('workbench_path')) {
    /**
     * Get the path to the workbench directory
     *
     * @param  string  $path
     * @return string
     */
    function workbench_path($path = '')
    {
        $workbenchPath = Path::join(app()->basePath(), config('workbench.workbench_dir'));
        return $path === '' ? $workbenchPath : Path::join($workbenchPath, $path);
    }
}
