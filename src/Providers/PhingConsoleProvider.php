<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * MIT License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Workbench\Providers;

use Sebwite\Support\Console\ConsoleServiceProvider;

/**
 * This is the WorkbenchConsoleProvider.
 *
 * @package        Sebwite\Dev
 * @author         Sebwite Dev Team
 * @copyright      Copyright (c) 2015, Sebwite
 * @license        https://tldrlegal.com/license/mit-license MIT License
 */
class PhingConsoleProvider extends ConsoleServiceProvider
{
    /**
     * @var string
     */
    protected $namespace = 'Sebwite\\Workbench\\Console\\Phing';

    /**
     * @var string
     */
    protected $prefix = 'command.workbench.phing.';

    /**
     * @var array
     */
    protected $commands = [
        'list'   => 'PhingList',
        'run'   => 'PhingRun'
    ];
}
