<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * MIT License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Workbench;

use Sebwite\Support\ServiceProvider;
use Sebwite\Workbench\Packages\Package;

/**
 * This is the WorkbenchServiceProvider.
 *
 * @author    Sebwite Dev Team
 * @copyright Copyright (c) 2015, Sebwite
 * @license   https://tldrlegal.com/license/mit-license MIT License
 */
class WorkbenchServiceProvider extends ServiceProvider
{
    protected $dir = __DIR__;

    protected $configFiles = [ 'workbench' ];

    protected $viewDirs = [ 'stubs' => 'workbench' ];

    protected $providers = [
        \Sebwite\Git\GitServiceProvider::class
    ];

    protected $deferredProviders = [
        Providers\WorkbenchConsoleProvider::class,
        Providers\PhingConsoleProvider::class
    ];

    protected $bindings = [
       # 'workbench.packages.package'    => Packages\Package::class,
       # 'workbench.packages.collection' => Packages\Collection::class
    ];

    protected $singletons = [
        'workbench' => Workbench::class
    ];

    protected $aliases = [
        'workbench' => Contracts\Workbench::class
    ];

    protected $helpers = [
        'register' => 'helpers.php'
    ];

    protected $provides = [ 'workbench' ];

    public function boot()
    {
        $app = parent::boot();

        return $app;
    }


    /**
     * {@inheritdoc}
     */
    public function register()
    {
        $app = parent::register();
        $this->app->bind('workbench.packages.package', Packages\Package::class);
        $this->registerWorkbenchComponents();
        $this->registerPackageComponents();
        return $app;
    }

    protected function registerWorkbenchComponents()
    {
        Workbench::extend('composer', \Sebwite\Workbench\Components\Composer::class);
        Workbench::extend('packages', \Sebwite\Workbench\Components\Packages::class);
    }

    protected function registerPackageComponents()
    {
        Package::extend('composer', \Sebwite\Workbench\Packages\Components\Composer::class);
        Package::extend('phing', \Sebwite\Workbench\Packages\Components\Phing::class);
        Package::extend('generator', \Sebwite\Workbench\Packages\Components\Generator::class);
        Package::extend('git', \Sebwite\Workbench\Packages\Components\Git::class);
    }


}
