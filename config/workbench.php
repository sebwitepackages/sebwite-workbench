<?php

return [
    'workbench_dir'  => 'workbench',
    'console_prefix' => 'wb',
    'copyright'      => 'Copyright (c) 2015, Sebwite',
    'license'        => 'MIT',
    'license_link'   => 'http://mit-license.org',
    'author'         => [
        'name'     => 'Sebwite',
        'email'    => 'info@sebwite.nl',
        'homepage' => 'https://sebwite.nl'
    ],
    'composer'       => [
        'project_file' => 'composer.json', // relative to project root
        'package_file' => 'composer.json'       // relative to package root
    ],
    'idea'           => [
        'path'                   => '.idea',
        'git_vcs_search_folders' => [ 'workbench', 'app' ],
        'resource_folders'       => [
            // namespace => glob search pattern (relative to workbench folder)
            // glob pattern accepts braces and is recursive
            'config' => [ '*/*/config' ],
            'views'  => [ '*/*/resources/views' ],
            'assets' => [ '*/*/resources/assets' ]
        ]
    ],
    'phing'          => [

    ],
    'git'            => [
        'fetch' => 'git -c core.quotepath=false fetch origin --verbose', //--progress --prune',
        'merge' => 'git -c core.quotepath=false merge origin/{branch} --no-stat --verbose'
    ],
    'generator'      => [
        // available vars: $package_path
        'stubs_path' => __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . 'stubs',
        'files'      => [
            'src/ServiceProvider.php.stub'                  => 'src/{classPrefix}ServiceProvider.php',
            #'src/Providers/ConsoleServiceProvider.php.stub' => 'src/Providers/ConsoleServiceProvider.php',
            #'src/Console/TestCommand.php.stub'              => 'src/Console/{classPrefix}TestCommand.php',
            'config/config.php.stub'                        => 'config/{configFileName}.php',
            #'tests/TestCase.php.stub'                       => 'tests/TestCase.php',
            #'tests/ServiceProviderTest.php.stub'            => 'tests/{classPrefix}ServiceProviderTest.php',
            #'docs/index.md.stub'                            => 'docs/index.md',
            #'docs/menu.yml.stub'                            => 'docs/menu.yml',
            'src/extension.php.stub' => 'src/extension.php',
            'src/Http/routes.php.stub' => 'src/Http/routes.php',
            'extension.php.stub'   => '_extension.php',
            'editorconfig.stub'    => '.editorconfig',
            'build.xml.stub'       => 'build.xml',
            'composer.json.stub'   => 'composer.json',
            'gitignore.stub'       => '.gitignore',
            #'LICENSE.md.stub'      => 'LICENSE.md',
            #'phpunit.xml.stub'     => 'phpunit.xml',
            #'README.md.stub'       => 'README.md',
            #'scrutinizer.yml.stub' => '.scrutinizer.yml',
            #'travis.yml.stub'      => '.travis.yml'
        ]
    ]
];
