<!---
title: Installation
author: Sebwite
-->

##### Required
1. Add to composer.json:
```js
"require": {
    "wikimedia/composer-merge-plugin": "~1.3",
    "radic/blade-extensions": "~6.0.4",
    "sebwite/workbench": "~1.0"
},
"extra": {
    "component": "application",
    "merge-plugin": {
        "include": [
            "composer.dev.json"
        ]
    }
}
```
2. Create a `composer.dev.json` next to your `composer.json`. Workbench will manage the `composer.dev.json` for your packages. 
3. Add to `config/app.php` service providers: `Sebwite\Workbench\WorkbenchServiceProvider::class`
4. Create a `workbench` directory in the root project directory.

##### Optional
`sebwite/workbench` uses `sebwite/git` to interact with the `bitbucket` and `github` api. 
It is highly recommended to add a **Github token** and **Bitbucket oauth** credentials to your `.env` file:
```bash
BITBUCKET_CLIENT_KEY=xxxxx
BITBUCKET_CLIENT_SECRET=xxxxx
GITHUB_TOKEN=xxxxx
```

This will enable *a lot* of extra functionality like cloning, creating repositories, organisation/repository browsing/selecting etc.. 
