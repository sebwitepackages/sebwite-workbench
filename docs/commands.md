<!---
title: Commands
author: Sebwite
-->


**Note:** most options/arguments are optional. You will be send trough a 'wizard' question list if there are any missing arguments.
 
#### Add package 
Generate a new package or clone from a bitbucket/github repository. The stubs and file structure are customisable.

**Arguments**
- *method (optional)*:      `new|clone`

**`wb:add [options] [--] [<method>]`**
- `wb:add`
- `wb:add new`
- `wb:add clone`
- `wb:add clone --org=laravel --repo=framework`
- `wb:add new --name=vendor/package`


#### Version bump (tag) a package
Version bump a package and tag it. Either ``.

**`wb:bump [options] [--] [<name>] [<type>]`**
- `wb:bump`
- `wb:bump sebwite/workbench`
- `wb:bump sebwite/workbench patch` 

Arguments (optional):
- **name**:      `vendor/package`
- **type**:      `patch|minor|major`

| Option | Default | Description |
|:-------|:--------|:------------|
| --push[=PUSH] | origin | The remote to push to |

##### wb:commit
Commit one or more packages. Will ask for a commit message and if you want to push it.

**`wb:commit [options] [--] [<name>]`**
- `wb:bump`
- `wb:bump sebwite/workbench`
- `wb:bump sebwite/workbench patch` 

Arguments (optional):
- **name**:      `vendor/package`

| Option | Default | Description |
|:-------|:--------|:------------|
| --push[=PUSH] | origin | The remote to push to |
| --all |  | Will commit all packages that have changes using the same commit message |


##### wb:commit

##### wb:edit

##### wb:info

##### wb:list

##### wb:idea:show

##### wb:idea:sync


