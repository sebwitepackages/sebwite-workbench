<!---
title: Overview
author: Sebwite
-->

The workbench package will manage the merging of your package's `composer.json`. 
This will make it so that all the dependencies defined in your packages will be also downloaded and installed for your root project, without clogging up your `composer.json` file.
It also allows your packages to define dependencies on each other, those will not be downloaded as they already exist in the workbench! 

This is mostly thanks to the `wikimedia/composer-merge-plugin` plugin. Which will merge all the defined `composer.json` files into the root one **on runtime**.

The workbench will provide you with:
- Composer merge plugin management. Add/remove packages composer file to the merge plugin.
- Generators and scaffolding, very customisable.
- Git helpers (eg: version bump, pre-commit code fixers, etc)
- PHPStorm/IDEA auto-configurator (source folders/namespacing, git vcs roots, etc). Such a timesaver after cloning 10 packages into the workbench!
- Phing task overview, run phing tasks, add pre-configured phing tasks to the build file. Add some tasks to some git-hooks.   
- API: Easy to understand, use and extend. Expect syntax like: `Workbench::package('my/package')->composer->get('require-dev')` 

