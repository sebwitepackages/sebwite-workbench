<!---
title: API usage
author: Sebwite
-->

#### Accessing the API

1. You can use the binding `workbench` or create a Facade for it (not included). 
```php
app('workbench')->shell('chmod +x file.sh');
Workbench::shell('chmod +x file.sh');
```

2. You can use the contract as typehint to inject it into your constructors.
```php
use Sebwite\Workbench\Contracts\Workbench;

public function __construct(Workbench $wb){
    $wb->shell('chmod +x file.sh');
}
```

## Workbench
```php
$wb = app('workbench');

// Execute a shell command, if no $path, will be executed in the project's root
$wb->shell($commands = [], $path = null);

// Get a config item from the config file
$wb->config($key = null, $default = null);

// Get the absolute path to the workbench directory. 
// If $path is provided, it will be the $path inside the worbench dir
$wb->path($path = null);

// Get an existing package instance
$package = $wb->package($packageName);

// Create a new package and return the package instance
$package = $wb->createPackage($packageName);

// Check if the given string is a valid semver
$valid = $wb->validateVersion($versionString);
```

#### Components

###### Composer

```php
$wb = app('workbench');

// Run composer commands
$wb->composer->run('dumpautoload');

//  Merge config into the composer.json file
$wb->composer->merge(array $composerData, $path = null);

// Get value from the composer file using dot notation
$wb->composer->get($key = null, $default = null, $path = null);

// Set values in the composer file using dot notation
$wb->composer->set($key, $value = null, $path = null);

// Get all the auo
$wb->composer->getAutoloads();
```

## Workbench

```php

```

#### Components

###### Composer

```php

```

###### Composer

```php

```

###### Composer

```php

```
